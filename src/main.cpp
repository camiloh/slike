#include <QQmlApplicationEngine>
#include <QCommandLineParser>
#include <QIcon>
#include <QDate>
#include <QFileInfo>

#ifdef Q_OS_ANDROID
#include <QGuiApplication>
#else
#include <QApplication>
#endif

#include <MauiKit/Core/mauiapp.h>
#include <KI18n/KLocalizedString>

#include "slike_version.h"

#define SLIKE_URI "org.maui.slike"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps, true);

#ifdef Q_OS_ANDROID
	QGuiApplication app(argc, argv);
	if (!MAUIAndroid::checkRunTimePermissions({"android.permission.WRITE_EXTERNAL_STORAGE"}))
		return -1;
#else
	QApplication app(argc, argv);
#endif

    app.setOrganizationName("Maui");
    app.setWindowIcon(QIcon(":/assets/slike.svg"));

    MauiApp::instance()->setIconName("qrc:/assets/slike.svg");

    KLocalizedString::setApplicationDomain("slike");
    KAboutData about(QStringLiteral("slike"), i18n("Slike"), SLIKE_VERSION_STRING, i18n("Dummy template to create future Maui Apps."),  KAboutLicense::LGPL_V3, i18n("© 2019-%1 Maui Development Team", QString::number(QDate::currentDate().year())));
    about.addAuthor(i18n("Camilo Higuita"), i18n("Developer"), QStringLiteral("milo.h@aol.com"));
    about.setHomepage("https://mauikit.org");
    about.setProductName("maui/slike");
    about.setBugAddress("https://invent.kde.org/camiloh/slike/-/issues");
    about.setOrganizationDomain(SLIKE_URI);
    about.setProgramLogo(app.windowIcon());

    KAboutData::setApplicationData(about);


	QQmlApplicationEngine engine;
	const QUrl url(QStringLiteral("qrc:/main.qml"));
	QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl)
	{
		if (!obj && url == objUrl)
			QCoreApplication::exit(-1);

	}, Qt::QueuedConnection);

    engine.load(url);

#ifdef Q_OS_MACOS
//    MAUIMacOS::removeTitlebarFromWindow();
//    MauiApp::instance()->setEnableCSD(true); //for now index can not handle cloud accounts
#endif

	return app.exec();
}
