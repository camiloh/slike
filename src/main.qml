import QtQuick 2.14
import QtQuick.Controls 2.14

import org.mauikit.controls 1.3 as Maui

Maui.ApplicationWindow
{
    id: root
    headBar.visible: false
    sideBar: Maui.AbstractSideBar
    {
        collapsed: !isWide
        preferredWidth: 200
        visible: true

        Maui.Page
        {
            anchors.fill: parent
            title: i18n("Global Sidebar")

            Maui.ListBrowser
            {
                anchors.fill: parent

                model: 20

                delegate: Maui.ListDelegate
                {
                    width: ListView.view.width
                    label: modelData
                }
            }

        }
    }

    footBar.middleContent: Label
    {
        text: i18n("Information label")
    }

    footBar.leftContent: ToolButton
    {
        icon.name: sideBar.visible ? "sidebar-collapse" : "sidebar-expand"
        onClicked: sideBar.visible ? sideBar.close() : sideBar.open()
    }

    footBar.rightContent: Maui.ToolButtonMenu
    {
        icon.name: "overflow-menu"

        MenuItem
        {
            text: "Menu entry 1"
        }

        MenuItem
        {
            text: "Menu entry 2"
        }

        MenuItem
        {
            text: "Menu entry 3"
        }
    }

    Maui.AppViews
    {
        anchors.fill: parent
        showCSDControls: true

        headBar.leftContent: Maui.ToolButtonMenu
        {
            icon.name: "application-menu"

            MenuItem
            {
                text: i18n("About")
                icon.name: "documentinfo"
                onTriggered: root.about()
            }
        }

        Player
        {
            Maui.AppView.title: "Main"
            Maui.AppView.iconName: "go-home"

        }

        Maui.Page
        {
            Maui.AppView.title: "Page"
            Maui.AppView.iconName: "headphones"

            Rectangle
            {
                anchors.centerIn: parent
                height: 60
                width: height
                color: "yellow"
            }
        }

        Maui.Page
        {
            Maui.AppView.title: "View"
            Maui.AppView.iconName: "love"

            Rectangle
            {
                anchors.centerIn: parent
                height: 60
                width: height
                color: "orange"
            }
        }
    }


}
