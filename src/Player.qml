import QtQuick 2.14
import QtQuick.Controls 2.14

import QtQuick.Layouts 1.3
import QtMultimedia 5.8

import org.mauikit.controls 1.2 as Maui

Maui.Page
{
    id: control
    property alias video : player
    property alias url : player.source
    readonly property bool playing : player.playbackState === MediaPlayer.PlayingState
    readonly property bool paused : player.playbackState === MediaPlayer.PausedState
    readonly property bool stopped : player.playbackState === MediaPlayer.StoppedState

    Video
    {
        id: player
        anchors.fill: parent
        autoLoad: true
        autoPlay: true
        focus: true
        Keys.onSpacePressed: player.playbackState == MediaPlayer.PlayingState ? player.pause() : player.play()
        Keys.onLeftPressed: player.seek(player.position - 5000)
        Keys.onRightPressed: player.seek(player.position + 5000)

        RowLayout
        {
            anchors.fill: parent

            MouseArea
            {
                Layout.fillWidth: true
                Layout.fillHeight: true
                onDoubleClicked: player.seek(player.position - 5000)
            }

            MouseArea
            {
                Layout.fillWidth: true
                Layout.fillHeight: true
                onClicked: player.playbackState === MediaPlayer.PlayingState ? player.pause() : player.play()
            }

            MouseArea
            {
                Layout.fillWidth: true
                Layout.fillHeight: true
                onDoubleClicked: player.seek(player.position + 5000)
            }
        }
    }

    footBar.leftContent: Maui.ToolActions
    {
        expanded: true
        Action
        {
            icon.name: "media-skip-backward"
        }

        Action
        {
            icon.name: player.playbackState === MediaPlayer.PlayingState ? "media-playback-pause" : "media-playback-start"
            onTriggered: player.playbackState === MediaPlayer.PlayingState ? player.pause() : player.play()
        }

        Action
        {
            icon.name: "media-skip-forward"
        }
    }

    footBar.rightContent: [
        Label
        {
            text: player.position
        }
    ]

    footBar.middleContent : Slider
    {
        id: _slider
        Layout.fillWidth: true
        orientation: Qt.Horizontal
        from: 0
        to: 1000
        value: (1000 * player.position) / player.duration

        onMoved: player.seek((_slider.value / 1000) * player.duration)
    }
}
